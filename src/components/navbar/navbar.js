import React from 'react';

const Navbar = () => (
    <nav className="navbar navbar-light shadow-sm navbar-expand-lg" style={{zIndex:99}}>
        <a className="navbar-brand flex-grow-1" href="#">
            <img src="/images/th.png" className="mr-2" />
            <img src="https://despatchcloud.com/img/logo.svg" width="200" />
        </a>
        <img src="/images/search-duotone.png" className="d-none d-lg-block"/>
        <input className="form-control border-0 d-none d-lg-block" type="search" placeholder="Search"/>
        <div className="d-flex flex-column mr-4 d-none d-lg-flex">
            <div className="font-weight-bold d-none d-lg-flex">Corey Cross</div>
            <div className="d-none d-lg-flex">Administrator</div>
        </div>
        <div className="avatar shadow">CC</div>
    </nav>);

export default Navbar;