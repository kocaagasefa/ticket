import React from 'react';
import classNames from './ticketList.module.css';
import TicketListItem from '../ticketListItem/ticketListItem';

const TicketList = () => (
    <div className={[classNames.ticket_list].join(" ")}>
        <div className="custom-control custom-checkbox mb-2">
            <input type="checkbox" className="custom-control-input" defaultChecked />
            <label className="custom-control-label" htmlFor="customCheck">Select All
            </label>
        </div>
        <div className={[classNames.date, "text-center"].join(" ")}>
        11th July 2019
        </div>
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <div className={[classNames.date, "text-center"].join(" ")}>
        11th July 2019
        </div>
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
        <TicketListItem />
    </div>
);

export default TicketList;