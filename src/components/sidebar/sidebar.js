import React from 'react';
import classNames from './sidebar.module.css';

const Sidebar = () => (<div className={["pt-2", classNames.sidebar].join(" ")}>
    <div className={["bg-aqua custom-sidebar text-light p-2 d-flex align-items-center flex-nowrap text-nowrap", classNames.new_ticket].join(" ")}>
        Create Ticket <div className="flex-grow-1"><img src="/images/plane.png" className="float-right mr-4" /></div>
    </div>
    <div className="p-4 d-flex justify-content-between align-items-center">
        <b className={[classNames.title].join(" ")}>Tickets
        </b>
        <span className={["badge badge-pill badge-secondary float-right", classNames.badge].join(" ")}>43</span>
    </div>
    <div className={[classNames.sidebar_item, classNames.item_active, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_aqua, "mr-2"].join(" ")}></div>
        <div>Open</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>43</div>
    </div>
    <div className={[classNames.sidebar_item, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_orange, "mr-2"].join(" ")}></div>
        <div>Awaiting Reply</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>2</div>
    </div>
    <div className={[classNames.sidebar_item, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_green, "mr-2"].join(" ")}></div>
        <div>Resolved</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>12</div>
    </div>
    <div className={[classNames.sidebar_item, classNames.item_active, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_grey, "mr-2"].join(" ")}></div>
        <div>Archived</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>12</div>
    </div>
    <div className="p-4 d-flex justify-content-between align-items-center">
        <b className={[classNames.title].join(" ")}>Priority
        </b></div>
    <div className={[classNames.sidebar_item, classNames.item_active, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_red, "mr-2"].join(" ")}></div>
        <div>Urgent</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>43</div>
    </div>
    <div className={[classNames.sidebar_item, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_orange, "mr-2"].join(" ")}></div>
        <div>High</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>2</div>
    </div>
    <div className={[classNames.sidebar_item, classNames.item_active, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_green, "mr-2"].join(" ")}></div>
        <div>Medium</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>43</div>
    </div>
    <div className={[classNames.sidebar_item, "mx-4 d-flex align-items-center"].join(" ")}>
        <div className={[classNames.circle, classNames.circle_aqua, "mr-2"].join(" ")}></div>
        <div>Low</div>
        <div className={["text-right flex-grow-1", classNames.item_count].join(" ")}>43</div>
    </div>
    <div className="p-4">
        <b className={[classNames.title].join(" ")}>Tags
        </b>
        <input className="form-control ds-input my-2" placeholder="Search for tag" />
        <div className="font-weight-light d-flex flex-wrap">
            <div className="mt-1">
                Trash
                <div className={[classNames.close, "rounded-circle d-inline-flex justify-content-center align-items-center ml-1"].join(" ")}>
                    <span> X</span>
                </div>
            </div>
            <div className="mt-1">
                Jackets
                <div className={[classNames.close, "rounded-circle d-inline-flex justify-content-center align-items-center ml-1"].join(" ")}>
                    <span> X</span>
                </div>
            </div>
            <div className="mt-1">
                Jackets
                <div className={[classNames.close, "rounded-circle d-inline-flex justify-content-center align-items-center ml-1"].join(" ")}>
                    <span> X</span>
                </div>
            </div>
            <div className="mt-1">
                Jumpers
                <div className={[classNames.close, "rounded-circle d-inline-flex justify-content-center align-items-center ml-1"].join(" ")}>
                    <span> X</span>
                </div>
            </div>
        </div>
    </div>
</div>);

export default Sidebar;