import React from 'react';
import classNames from './ticketListItem.module.css';

const TicketListItem = () => (<div className={["d-flex align-items-center", classNames.container].join(" ")}>
    <div className="custom-control custom-checkbox mb-2">
        <input type="checkbox" className="custom-control-input" defaultChecked />
        <label className="custom-control-label" htmlFor="customCheck"></label>
    </div>
    <div className={[classNames.ticket_list_item, "d-flex align-items-stretch flex-grow-1"].join(" ")}  >
        <div className={[classNames.edge].join(" ")} />
        <div className="p-3 d-flex align-items-center flex-grow-1  justify-content-start justify-content-lg-between">
            <div className={classNames.icon}></div>
            <div className="d-flex flex-column ml-2">
                <div className={[classNames.name].join(" ")}>Matthew Dunne</div>
                <div className={[classNames.type]}>
                    Migration to Shopify <span className={classNames.code}> (#4345)</span>
                </div>
            </div>
            <div className={[classNames.desc, " text-center d-none d-lg-block"].join(" ")}>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr
            </div>
            <div className={[classNames.priority, "mx-3 text-center d-none d-lg-block"].join(" ")}>
                Medium
            </div>
            <div className={[classNames.time, "mx-3 text-center d-none d-lg-block"].join(" ")}>
                12 hours ago
            </div>
            <div className="avatar shadow ml-3 d-none d-lg-block">CC</div>
        </div>
    </div>
</div>)

export default TicketListItem;