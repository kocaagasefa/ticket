import React, { useState } from 'react';
import Navbar from '../navbar/navbar';
import Sidebar from '../sidebar/sidebar';
import classNames from './layout.module.css';

const Layout = ({ children }) => {
    const [active, setActive] = useState(false);

    return (<div>
        <Navbar />
        <div className={["d-flex", classNames.layout].join(" ")}>
            <div className={[classNames.sidebar_container, active ? classNames.active : ""].join(" ")}>
                <Sidebar />
                {active && <div className={[classNames.filter,"d-block d-lg-none"].join(" ")} onClick={() => setActive(!active)} >
                    Filter
                </div>}
            </div>
            <div className="flex-grow-1">
                {children}
            </div>
        </div>
        {!active && <div className={[classNames.filter, classNames.filter_closed,"d-block d-lg-none"].join(" ")} onClick={() => setActive(!active)} >
            Filter
        </div>}
        
    </div>)
}

export default Layout;