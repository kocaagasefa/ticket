import React from 'react';
import './App.css';
import './common/style.css';
import Layout from './components/layout/layout';
import TicketList from './components/ticketList/ticketList';

function App() {
  return (
    <Layout>
      <div className="d-flex align-items-stretch">
      <TicketList />
      <div className="align-items-center justify-content-center d-none d-lg-flex" style={{width:"33%"}}>
        <div className="">
          <img src="/images/empty.png" className="mx-4"/>
        </div>
      </div>
      </div>
    </Layout>
  );
}

export default App;
